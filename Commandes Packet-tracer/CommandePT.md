# Projet-Reseau-3IBD


# Réseau d'entreprise 2 (Comptabilité et finance)


## Switch S1

```
Switch>hostname S1
S1>enable
--- Mode privilégié
S1#configure terminal 
--- Mode configuration
```
Nommage du VLAN en VLAN 10
```
S1(config)#vlan 10 
--- Création vlan avec un identité
S1(config-vlan)#name VLAN10 
--- Création du nom du vlan
S1(config-vlan)#exit
```
Nommage du VLAN en VLAN 20
```
S1(config)#vlan 20 
--- Création vlan avec un identité
S1(config-vlan)#name VLAN20 
--- Création du nom du vlan
S1(config-vlan)#exit
```
Configuration des VLAN 10 et VLAN 20
```
S1(config)#interface range fastEthernet 0/3-4 
--- Configuration des ports 3-4
S1(config-if-range)#switchport mode trunk 
--- Liste des vlan autorisés sur la liaison trunk
```

```
S1(config-if-range)#switchport trunk allowed vlan 10,20 
--- Liste des vlan autorisés sur la liaison trunk
S1(config-if-range)#exit
```



## Switch S2

```
Switch>hostname S2
S2>enable
--- Mode privilégié
S2#configure terminal 
--- Mode configuration
```
Nommage du VLAN en VLAN 10
```
S2(config)#vlan 10 
--- Création vlan avec un identité
S2(config-vlan)#name VLAN10 
--- Création du nom du vlan
S2(config-vlan)#exit
```
Nommage du VLAN en VLAN 20
```
S2(config)#vlan 20 
--- Création vlan avec un identité
S2(config-vlan)#name VLAN20 
--- Création du nom du vlan
S2(config-vlan)#exit
```
Configuration des VLAN 10 et VLAN 20
```
S2(config)#interface range fastEthernet 0/1-2 
--- Configuration des ports 1-2
S2(config-if-range)#switchport mode access 
--- Active le mode access
S2(config-if-range)#switchport access vlan 10
--- Accès au vlan 10 
S2(config-if-range)#exit
```

```
S2(config)#interface range fastEthernet 0/3-4 
--- Configuration des ports 3-4
S2(config-if-range)#switchport mode access 
--- Active le mode access
S2(config-if-range)#switchport access vlan 20
--- Accès au vlan 20 
S2(config-if-range)#exit
```

```
S2(config)#interface fastEthernet 0/5 
--- Accès au configuration de l’interface pour le port 5
S2(config-if)#switchport mode trunk 
--- Liste des vlan autorisés sur la liaison trunk
S2(config-if)#switchport trunk allowed vlan 10,20 
--- Liste des vlan autorisés sur la liaison trunk
S2(config-if)#exit
```

## Switch S3

```
Switch>hostname S3
S3>enable
--- Mode privilégié
S3#configure terminal
--- Mode configuration
```

Nommage du VLAN en VLAN 10
```
S3(config)#vlan 10
--- Création du vlan avec une identité
S3(config-vlan)#name VLAN10
--- Nommage du vlan
S3(config-vlan)#exit
```
Nommage du VLAN en VLAN 20
```
S3 (config)#vlan 20
--- Création du vlan avec une identité
S3 (config-vlan)#name VLAN20
--- Nommage du vlan
S3 (config-vlan)#exit
```
Configuration des VLAN 10 et VLAN 20
```
S3(config)#interface range fastEthernet 0/1-2
--- Configuration des ports 1 à 2
S3(config-if-range)#switchport mode access 
--- Active le mode access
S3(config-if-range)#switchport access vlan 10
--- Acces au vlan 10
S3(config-if-range)#exit
```

```
S3 (config)#interface range fastEthernet 0/3-4
--- Configuration des ports 3 à 4
S3 (config-if-range)#switchport mode access
--- Active le mode access
S3 (config-if-range)#switchport access vlan 20
--- Acces au vlan 20
S3 (config-if-range)#exit
```


```
S3 (config)#interface fastEthernet 0/5
--- Access au configuration de l’interface pour le port 5
S3 (config-if)#switchport mode trunk 
--- Forcer la liaison à devenir une liaison trunk
S3 (config-if)#switchport trunk allowed vlan 10,20
--- Liste des vlan autorisés sur la liaison trunk
S3 (config-if)#exit
```



## Routeur R1

```
Routeur>hostname R1
R1>enable 
--- Mode privilégié
R1#>configure terminal
--- Mode configuration
```

```
R1(config)#interface GigabitEthernet 0/0
--- Accès à l’interface 0/0 du routeur R1
R1(config-if)#no shutdown 
--- Active l’interface gigabitEthernet 0/0
R1(config-if)#ip address 124.63.32.1 255.255.255.0 
--- Affecte l’adresse IP 192.168.32.1 à l’interface g0/0
R1(config-if)#exit
```
```
R1(config)#interface GigabitEthernet 0/1 
--- Accès à l’interface 0/1 du routeur R1
R1(config-if)#no shutdown 
--- Active l’interface gigabitEthernet 0/1
R1(config-if)#ip address 192.168.40.2 255.255.255.0
--- Affecte l’adresse IP 192.168.40.2 à l’interface g0/1
R1(config-if)#exit
```




# Réseau d'entreprise 1 (Service DT, Service RH et Service Support)

## Routeur R2
```
Routeur>hostname R2
R2>enable 
--- Mode privilégié
R2#>configure terminal
--- Mode configuration
```

```
R2(config)#interface GigabitEthernet 0/1 
--- Accès à l’interface 0/1 du routeur R2
R2(config-if)#no shutdown 
--- Active l’interface gigabitEthernet 0/1
R2(config-if)#ip address 192.168.0.1 255.255.255.0 
--- Affecte l’adresse IP 192.168.0.1 à l’interface g0/1
```

```
R2(config-if)#exit
R2(config)#interface GIgabitEthernet 0/2 
--- Accès à l’interface 0/2 du routeur R2
R2(config-if)#no shutdown 
--- Active l’interface gigabitEthernet 0/2
R2(config-if)#ip address 138.63.32.1 255.255.255.0 
--- Affecte l’adresse IP 138.63.32.1 à l’interface g0/2
R2(config-if)#exit
```

Mise en place du routage statique
```
R2(config)#ip route 192.168.0.0 255.255.255.0 gigabitEthernet 0/1 
--- Ajoute une route statique pour joindre le réseau distant 192.168.0.0 en passant par GigabitEthernet 0/1
R2(config)#ip route 192.168.30.0 255.255.255.0 GigabitEthernet 0/2
--- Ajoute une route statique pour joindre le réseau distant 192.168.30.0 en passant par GigabitEthernet 0/2
R2(config-if)#exit
```
Mise en place du NAT
```
R2(config)#int range GI 0/1-2
--- Permet d’exécuter des commandes sur plusieurs sous-interfaces
R2(config-if-range)#ip nat inside
--- Détermine une interface interne pour le NAT
R2(config-if-range)#exit
```

```
R2(config)#int GI 0/0
--- Permet d’exécuter des commandes sur plusieurs sous-interfaces
R2(config-if)#ip nat outside
--- Détermine une interface externe pour le NAT
R2(config-if)#exit
```

```
R2(config)#ip route 0.0.0.0 0.0.0.0 138.63.32.2  
--- Ajoute une route statique pour joindre tous les réseaux distants en passant par 138.163.32.2
```
Autorisation des 2 réseaux à sortir :
```
R2(config)#access-list 1 permit 192.168.30.0 0.0.0.255
R2(config)#access-list 1 permit 192.168.0.0 0.0.0.255
--- Ajout des 2 réseaux ci-dessus en autorisation dans la liste d’accès 1
R2(config)#ip nat inside source list 1 interface GigabitEthernet 0/0
--- Activer le NAT sur mon routeur en utilisant les autorisations de la liste d’acces 1 et en faisant sortir le tout avec l’adresse IP de l’interface face à internet g 0/0 (publique).
```

Mise en place du TELNET
```
R2(config)#username entrepriseBat1 secret bat1
--- Créer un couple login/mot de passe dans la configuration du routeur
```

Etablir le nombre de connexion
```
R2(config)#line vty 0 15
--- Configuration des terminaux virtuels de 0 à 15
R2(config-line)#transport input telnet 
--- Active le protocle Telnet
R2(config-line)#login local 
--- Utilise la base de données utilisateur en local
R2(config-line)#exit
```

```
Le mot de passe pour TELNET 
R2(config)#enable secret bat1
--- Mot de passe de connexion
```


## Routeur R3 et Switch S6 (Partie concernant le Service RH)

Nous avons pas réussit à faire un allouage dynamique des adresses IP sur cette partie donc voici quand même les commandes 

```
Routeur>hostname R3
R3>enable 
R3#configure terminal 
```
```
R3(config)#ip dhcp pool LAN-192.168.20.1
R3(dhcp-config)#network 192.168.20.0 255.255.255.0
R3(dhcp-config)#default-router 192.168.20.1
R3(dhcp-config)#dns-server 8.8.8.8 
R3(dhcp-config)#lease 1 0 0
--- Définit le temps maximum que l'adresse IP est valide (Jour, heure, minute)
```
```
R3(config)#ip dhcp excluded-address 192.168.20.1 192.168.20.6
R3(config)#ip dhcp excluded-address 192.168.20.254
```
```
R3(config)#interface GigabitEthernet0/1
R3(config-if)#ip address 192.168.20.1 255.255.255.0
R3(config-if)#exit

R3(config)#ip route 136.60.10.0 255.255.255.0 138.63.32.1

R2(config-if)#ip route 0.0.0.0 0.0.0.0 192.168.20.6
```


# Routeur 0 et Serveur WEB

```
Routeur>hostname R0
R0>enable 
R0#configure terminal 
```
```
R0(config)#interface GigabitEthernet0/0 
--- Accès à l’interface 0/0 du routeur R0
R0(config-if)#no shutdown 
--- Active l’interface gigabitEthernet 0/0
R0(config-if)#ip address 124.63.32.2 255.255.255.0 
--- Affecte l’adresse IP 124.63.32.2 à l’interface g0/0
```
```
R0(config)#interface GigabitEthernet0/1 
--- Accès à l’interface 0/1 du routeur R0
R0(config-if)#no shutdown 
--- Active l’interface gigabitEthernet 0/1
R0(config-if)#ip address 138.63.32.2 255.255.255.0 
--- Affecte l’adresse IP 138.63.32.2 à l’interface g0/1
```
```
Router(config)#interface GigabitEthernet0/2
--- Accès à l’interface 0/2 du routeur R0 
Router(config-if)#no shutdown 
--- Active l’interface gigabitEthernet 0/0
Router(config-if)#ip address 136.60.10.2 255.255.255.0 
--- Affecte l’adresse IP 136.60.10.2 à l’interface g0/2
```

Mise en place du routage statique 
```
R0(config)#ip route 138.63.32.0 255.255.255.0 GigabitEthernet 0/1
--- Ajoute une route statique pour joindre le réseau distant 138.63.32.0 en passant par GigabitEthernet 0/1
R0(config)#ip route 192.168.30.0 255.255.255.0 GigabitEthernet 0/1 
--- Ajoute une route statique pour joindre le réseau distant 192.168.30.0 en passant par GigabitEthernet 0/1
R0(config)#ip route 192.168.0.0 255.255.255.0 GigabitEthernet 0/1 
--- Ajoute une route statique pour joindre le réseau distant 192.168.0.0 en passant par GigabitEthernet 0/1
```